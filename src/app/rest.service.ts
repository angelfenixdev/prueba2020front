import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private http: HttpClient) { }

  getUsers():Observable<any>
  {
   
      return  this.http.get('http://localhost:3000/getUsers');
  
  }

  downloadUserData(u):Observable<any>
  {
   
      return  this.http.post('http://localhost:3000/downloadUserData',{user:u});
  
  }
}
