import { RestService } from './rest.service';
import { Component } from '@angular/core';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'front';
  users: any[] = []
  meta:any=null
  isPostVisible: boolean[] = []
  constructor(private restService: RestService) {
    this.restService.getUsers().subscribe((response) => {
      this.meta = response.meta
      this.users = response.users
      this.initVisiblePosts();
    });
  }

  togglePosts(index) {
    if (this.isPostVisible[index] == true) {
      this.isPostVisible[index] = false
    } else {
      this.isPostVisible[index] = true
    }
  }

  initVisiblePosts() {
    for (var i = 0; i < this.users.length; i++) {
      this.isPostVisible.push(true);
    }
  }

  downloadUserData(u) {
    this.restService.downloadUserData(u).subscribe((response) => {
      var filename = u.id + ' - ' + u.nombre + '.json'
      var blob = new Blob([JSON.stringify(response, null, 2)]);
      saveAs(blob, filename);
    });
  }
}

